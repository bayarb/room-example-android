package com.bogdanov.bayar.roomsqlexample.base;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.View;
import android.view.inputmethod.InputMethodManager;

import butterknife.ButterKnife;

import static android.app.Activity.RESULT_OK;

/**
 * Created by qqq on 11.08.2017.
 */

public abstract class BaseFragment extends Fragment implements BaseView {
    private static final String TAG = BaseFragment.class.getName();
    public static final int AUTH_CODE = 6;

    protected abstract void initViews();

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ButterKnife.bind(this, view);
        initViews();
    }


    public void hideKeyboard(){
        if (getActivity() != null) {
            Log.i(TAG, "hideKeyboard: ");
            InputMethodManager inputMethodManager = (InputMethodManager) getActivity().getSystemService(Activity.INPUT_METHOD_SERVICE);
            inputMethodManager.hideSoftInputFromWindow(getActivity().getCurrentFocus().getWindowToken(), 0);
        }
    }

    public void setTitle(String title){
        ((BaseActivity)getActivity()).getSupportActionBar().setTitle(title);
    }

    public void replaceFragment(int idContainer, Fragment fragment){
        getFragmentManager().beginTransaction().replace(idContainer, fragment, fragment.getClass().getName().toString()).commit();
    }

    public void replaceFragmentWithBackstack(int idContainer, Fragment fragment){
        getFragmentManager().beginTransaction()
                .replace(idContainer, fragment, fragment.getClass().getName().toString())
                .addToBackStack(fragment.getClass().getName().toString())
                .commit();
    }

    public void showDialogProgress(){
        if (getActivity()!=null)
            ((BaseActivity)getActivity()).showDialogProgress();
    }

    public void dismissDialogProgress(){
        if (getActivity()!=null)
            ((BaseActivity) getActivity()).dismissDialogProgress();

    }

    public boolean isOnline() {
        return getActivity() != null && ((BaseActivity) getActivity()).isOnline();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == AUTH_CODE && resultCode == RESULT_OK){
            initViews();
        }
    }

    public void showSoftKeyboard(View view) {
        if (getActivity() != null) {
            view.setFocusableInTouchMode(true);
            view.setFocusable(true);
            if (getActivity().getCurrentFocus() != null) {
                getActivity().getCurrentFocus().clearFocus();
            }
            if(view.requestFocus()){
                Log.i(TAG, "showSoftKeyboard: ");
                InputMethodManager imm =(InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.showSoftInput(view,InputMethodManager.SHOW_IMPLICIT);
            }

            if (getActivity().getCurrentFocus() == null) {
                Log.e(TAG, "showSoftKeyboard: focus not set!");
            } else {
                Log.i(TAG, "showSoftKeyboard: focus set successful");
            }
        }
    }

    public void hideSoftKeyboard() {
        if (getActivity() != null) {
            Log.i(TAG, "hideSoftKeyboard: ");
            InputMethodManager imm = (InputMethodManager)getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
            if (getActivity().getCurrentFocus() == null) {
                Log.e(TAG, "hideSoftKeyboard: there is no focus");
                imm.hideSoftInputFromInputMethod(new View(getActivity()).getWindowToken(), 0);
            } else {
                imm.hideSoftInputFromWindow(getActivity().getCurrentFocus().getWindowToken(), 0);
                getActivity().getCurrentFocus().clearFocus();
            }
        }
    }

    public void showSoftKeyboardTest(View view) {
        if (getActivity() != null) {
            Log.i(TAG, "showSoftKeyboardTest: ");
            ((BaseActivity) getActivity()).showSoftKeyboard(view);
        }
    }
}
