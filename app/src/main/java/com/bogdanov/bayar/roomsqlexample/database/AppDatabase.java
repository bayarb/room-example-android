package com.bogdanov.bayar.roomsqlexample.database;

import android.arch.persistence.db.SupportSQLiteOpenHelper;
import android.arch.persistence.room.Database;
import android.arch.persistence.room.DatabaseConfiguration;
import android.arch.persistence.room.InvalidationTracker;
import android.arch.persistence.room.RoomDatabase;

import com.bogdanov.bayar.roomsqlexample.model.User;

import java.util.List;

import io.reactivex.Observable;

/**
 * Created by qqq on 31.07.2017.
 */
@Database(entities = {User.class}, version = 1)
public abstract class AppDatabase extends RoomDatabase {

    private static final String TAG = AppDatabase.class.getName();

    public abstract UserDao userDao();

    @Override
    protected SupportSQLiteOpenHelper createOpenHelper(DatabaseConfiguration config) {
        return null;
    }

    @Override
    protected InvalidationTracker createInvalidationTracker() {
        return null;
    }

    public Observable<List<User>> getAllUsers() {
        return Observable.create(e -> {
            try {
                e.onNext(userDao().getAll());
                e.onComplete();
            } catch (Exception exception) {
                e.onError(exception);
            }
        });
    }

    public Observable<User> writeUser(User user) {
        return Observable.create(emitter -> {
            try {
                long id = userDao().writeUser(user);
                user.setUid(id);
                emitter.onNext(user);
                emitter.onComplete();
            } catch (Exception e) {
                emitter.onError(e);
            }
        });
    }

    public Observable<List<User>> writeAllUsers(List<User> users) {
        return Observable.create(emitter -> {
           try {
               userDao().writeUsers(users);
               emitter.onNext(users);
               emitter.onComplete();
           } catch (Exception e) {
               emitter.onError(e);
           }
        });
    }

    public Observable<User> deleteUser(User user) {
        return Observable.create(emitter -> {
            try {
                userDao().delete(user);
                emitter.onNext(user);
                emitter.onComplete();
            } catch (Exception e) {
                emitter.onError(e);
            }
        });
    }

    public Observable<User> updateUser(User user) {
        return Observable.create(emitter -> {
            try {
                userDao().updateUser(user);
                emitter.onNext(user);
                emitter.onComplete();
            } catch (Exception e) {
                emitter.onError(e);
            }
        });
    }
}
