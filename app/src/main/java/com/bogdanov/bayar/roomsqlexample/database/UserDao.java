package com.bogdanov.bayar.roomsqlexample.database;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.Query;
import android.arch.persistence.room.Update;

import com.bogdanov.bayar.roomsqlexample.model.User;

import java.util.List;

/**
 * Created by qqq on 31.07.2017.
 */

@Dao
public interface UserDao {

    @Query("SELECT * FROM user")
    List<User> getAll();

    @Query("SELECT * FROM user WHERE first_name LIKE :firstName AND last_name LIKE :lastName")
    User findByName(String firstName, String lastName);

    @Query("SELECT COUNT (*) from user")
    int countUsers();

    @Insert
    long writeUser(User user);

    @Insert
    void writeUsers(List<User> users);

    @Update
    void updateUser(User user);

    @Delete
    void delete(User user);

}
