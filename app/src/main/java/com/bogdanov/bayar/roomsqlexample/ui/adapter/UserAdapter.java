package com.bogdanov.bayar.roomsqlexample.ui.adapter;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.bogdanov.bayar.roomsqlexample.R;
import com.bogdanov.bayar.roomsqlexample.model.User;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import ru.rambler.libs.swipe_layout.SwipeLayout;

/**
 * Created by qqq on 02.08.2017.
 */

public class UserAdapter extends RecyclerView.Adapter<UserAdapter.UserViewHolder> {

    public interface UserAdapterListener {
        void onUserUpdate(User user);
        void onUserDelete(User user);
    }

    private List<User> mUserList;
    private SwipeLayout mGlobalSwipeLayout;
    private UserAdapterListener mListener;

    public UserAdapter(List<User> userList) {
        mUserList = userList;
    }

    @Override
    public UserViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_user, parent, false);
        return new UserViewHolder(view);
    }

    @Override
    public void onBindViewHolder(UserViewHolder holder, int position) {
        holder.onBind(mUserList.get(position));
    }

    @Override
    public int getItemCount() {
        return mUserList.size();
    }

    public void setListener(UserAdapterListener listener) {
        mListener = listener;
    }

    public void notifyUsersAdded(List<User> users) {
        notifyItemRangeInserted(0, users.size());
        resetGlobalSwipeLayout();
    }

    public void notifyUserAdded() {
        notifyItemInserted(mUserList.size() - 1);
        resetGlobalSwipeLayout();
    }

    public void notifyUserUpdated(int position) {
        notifyItemChanged(position);
        resetGlobalSwipeLayout();
    }

    public void notifyUserDeleted(int position) {
        notifyItemRemoved(position);
        resetGlobalSwipeLayout();
    }

    private void resetGlobalSwipeLayout() {
        if (mGlobalSwipeLayout != null)
            mGlobalSwipeLayout.animateReset();
    }


    public class UserViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.swipe_layout)
        SwipeLayout mCurrentSwipeLayout;

        @BindView(R.id.tv_id)
        TextView mIdTextView;

        @BindView(R.id.tv_name)
        TextView mNameTextView;

        @BindView(R.id.tv_age)
        TextView mAgeTextView;

        @BindView(R.id.tv_update)
        TextView mUpdateTextView;

        @BindView(R.id.tv_delete)
        TextView mDeleteTextView;

        public UserViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }

        public void onBind(User user) {

            mCurrentSwipeLayout.setOnSwipeListener(new SwipeLayout.OnSwipeListener() {
                @Override
                public void onBeginSwipe(SwipeLayout swipeLayout, boolean moveToRight) {
                    if (mGlobalSwipeLayout != null && mGlobalSwipeLayout != swipeLayout) {
                        mGlobalSwipeLayout.animateReset();
                    }
                    mGlobalSwipeLayout = swipeLayout;
                }

                @Override
                public void onSwipeClampReached(SwipeLayout swipeLayout, boolean moveToRight) {

                }

                @Override
                public void onLeftStickyEdge(SwipeLayout swipeLayout, boolean moveToRight) {

                }

                @Override
                public void onRightStickyEdge(SwipeLayout swipeLayout, boolean moveToRight) {

                }
            });

            mIdTextView.setText(String.valueOf(user.getUid()));
            mNameTextView.setText(String.format("%s %s", user.getFirstName(), user.getLastName()));
            mAgeTextView.setText(String.valueOf(user.getAge()));

            mUpdateTextView.setOnClickListener(view -> {
                if (mListener != null) {
                    mListener.onUserUpdate(user);
                    mCurrentSwipeLayout.animateReset();
                    resetGlobalSwipeLayout();
                }
            });

            mDeleteTextView.setOnClickListener(view -> {
                if (mListener != null) {
                    mListener.onUserDelete(user);
                    mCurrentSwipeLayout.animateReset();
                    resetGlobalSwipeLayout();
                }
            });
        }
    }
}
