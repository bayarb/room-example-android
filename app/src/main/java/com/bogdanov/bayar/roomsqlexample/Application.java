package com.bogdanov.bayar.roomsqlexample;

import android.content.Context;

import com.bogdanov.bayar.roomsqlexample.di.AppComponent;
import com.bogdanov.bayar.roomsqlexample.di.AppModule;
import com.bogdanov.bayar.roomsqlexample.di.DaggerAppComponent;

/**
 * Created by qqq on 31.07.2017.
 */

public class Application extends android.app.Application {
    private AppComponent mAppComponent;

    @Override
    public void onCreate() {
        super.onCreate();
        mAppComponent = createComponent();
    }

    private AppComponent createComponent() {
        return DaggerAppComponent.builder()
                .appModule(new AppModule(this))
                .build();
    }

    public static AppComponent getComponent(Context context) {
        return ((Application) context.getApplicationContext()).mAppComponent;
    }
}
