package com.bogdanov.bayar.roomsqlexample.ui.view;

import com.bogdanov.bayar.roomsqlexample.base.BaseView;
import com.bogdanov.bayar.roomsqlexample.model.User;

import java.util.List;

/**
 * Created by qqq on 31.07.2017.
 */

public interface MainView extends BaseView {
    void onGetAllUsersSuccess(List<User> users);

    void onDeleteUserSuccess(User user);

    void onDeleteUserError(Throwable throwable);
}
