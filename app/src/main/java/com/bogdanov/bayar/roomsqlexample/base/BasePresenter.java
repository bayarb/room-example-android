package com.bogdanov.bayar.roomsqlexample.base;

/**
 * Created by qqq on 31.07.2017.
 */

public abstract class BasePresenter <V extends BaseView> {

    private V mView;

    public final void attachView(V view) {
        if (view == null) {
            throw new NullPointerException("View must not be null");
        }
        mView = view;
    }

    protected final void detachView() {
        mView = null;
    }

    protected final V getView() {
        return mView;
    }

    protected final boolean isViewAttached() {
        return mView != null;
    }
}
