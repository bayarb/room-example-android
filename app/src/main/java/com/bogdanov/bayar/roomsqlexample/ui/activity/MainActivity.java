package com.bogdanov.bayar.roomsqlexample.ui.activity;

import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;

import com.bogdanov.bayar.roomsqlexample.Application;
import com.bogdanov.bayar.roomsqlexample.R;
import com.bogdanov.bayar.roomsqlexample.base.BaseActivity;
import com.bogdanov.bayar.roomsqlexample.model.User;
import com.bogdanov.bayar.roomsqlexample.presenter.MainPresenter;
import com.bogdanov.bayar.roomsqlexample.ui.adapter.UserAdapter;
import com.bogdanov.bayar.roomsqlexample.ui.dialog.AddUserDialog;
import com.bogdanov.bayar.roomsqlexample.ui.view.MainView;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;

public class MainActivity extends BaseActivity
        implements MainView, AddUserDialog.AddUserDialogListener, UserAdapter.UserAdapterListener {

    private static final String TAG = MainActivity.class.getName();

    @Inject
    MainPresenter mPresenter;

    @BindView(R.id.am_recyclerview)
    RecyclerView mRecyclerView;

    @BindView(R.id.am_fab)
    FloatingActionButton mFloatingActionButton;

    private UserAdapter mAdapter;
    private List<User> mUserList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Application.getComponent(this).inject(this);
        mPresenter.attachView(this);
        initViews();
    }

    @Override
    protected void initViews() {
        mUserList = new ArrayList<>();
        mAdapter = new UserAdapter(mUserList);
        mAdapter.setListener(this);
        mRecyclerView.setAdapter(mAdapter);
        mRecyclerView.setLayoutManager(new LinearLayoutManager(this));
        mPresenter.getAllUsers();
        mFloatingActionButton.setOnClickListener(view -> {
            AddUserDialog dialog = AddUserDialog.newInstance(AddUserDialog.ADD, null);
            dialog.setListener(this);
            dialog.show(getSupportFragmentManager(), AddUserDialog.class.getName());
        });
    }

    @Override
    public void onGetAllUsersSuccess(List<User> users) {
        Log.i(TAG, "onGetAllUsersSuccess: " + users.size());
        mUserList.addAll(users);
        mAdapter.notifyUsersAdded(users);
    }

    @Override
    public void onDeleteUserSuccess(User user) {
        Log.i(TAG, "onDeleteUserSuccess: " + user.toString());
        int position = mUserList.indexOf(user);
        if (position != -1) {
            mUserList.remove(user);
            mAdapter.notifyUserDeleted(position);
        }
    }

    @Override
    public void onDeleteUserError(Throwable throwable) {
        Log.e(TAG, "onDeleteUserError: " + throwable.getMessage());
        throwable.printStackTrace();
    }

    // UserAddDialogListener
    @Override
    public void onUserAdded(User user) {
        Log.i(TAG, "onUserAdded: " + user.toString());
        mUserList.add(user);
        mAdapter.notifyUserAdded();
        mRecyclerView.smoothScrollToPosition(mUserList.size());
    }

    @Override
    public void onUserUpdated(User user) {
        Log.i(TAG, "onUserUpdated: " + user.toString());
        int position = mUserList.indexOf(user);
        if (position != -1)
            mAdapter.notifyUserUpdated(position);
    }

    // UserAdapterListener
    @Override
    public void onUserUpdate(User user) {
        AddUserDialog dialog = AddUserDialog.newInstance(AddUserDialog.UPDATE, user);
        dialog.setListener(this);
        dialog.show(getSupportFragmentManager(), AddUserDialog.class.getName());
    }

    @Override
    public void onUserDelete(User user) {
        mPresenter.deleteUser(user);
    }
}
