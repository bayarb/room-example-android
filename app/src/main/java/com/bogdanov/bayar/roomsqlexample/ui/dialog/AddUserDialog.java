package com.bogdanov.bayar.roomsqlexample.ui.dialog;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.bogdanov.bayar.roomsqlexample.Application;
import com.bogdanov.bayar.roomsqlexample.R;
import com.bogdanov.bayar.roomsqlexample.model.User;
import com.bogdanov.bayar.roomsqlexample.presenter.AddUserPresenter;
import com.bogdanov.bayar.roomsqlexample.ui.view.AddUserDialogView;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by qqq on 02.08.2017.
 */

public class AddUserDialog extends DialogFragment implements AddUserDialogView {

    private static final String TAG = AddUserDialog.class.getName();

    private static final String OPERATION_NAME_EXTRA = "operation_name";
    private static final String USER_EXTRA = "user";

    public static final String UPDATE = "update";
    public static final String ADD = "add";

    public static AddUserDialog newInstance(String operation, User user) {
        AddUserDialog dialog = new AddUserDialog();
        Bundle bundle = new Bundle();
        bundle.putString(OPERATION_NAME_EXTRA, operation);
        if (user != null)
            bundle.putSerializable(USER_EXTRA, user);
        dialog.setArguments(bundle);
        return dialog;
    }

    public interface AddUserDialogListener {
        void onUserAdded(User user);
        void onUserUpdated(User user);
    }

    @BindView(R.id.et_first_name)
    EditText mFirstNameEditText;

    @BindView(R.id.et_last_name)
    EditText mLastNameEditText;

    @BindView(R.id.et_age)
    EditText mAgeEditText;

    @BindView(R.id.btn_add_user)
    Button mAddUserButton;

    @Inject
    AddUserPresenter mPresenter;

    private User mUser;
    private AddUserDialogListener mListener;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.dialog_add_user, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        Application.getComponent(getActivity()).inject(this);
        mPresenter.attachView(this);
        ButterKnife.bind(this, view);
        initViews();
    }

    private void initViews() {
        String operation = getArguments().getString(OPERATION_NAME_EXTRA, "");
        if (operation.equals(UPDATE)) {
            mAddUserButton.setText("Изменить");
            mUser = ((User) getArguments().get(USER_EXTRA));
            if (mUser != null) {
                Log.i(TAG, "initViews: " + mUser.toString());
                mFirstNameEditText.setText(mUser.getFirstName());
                mLastNameEditText.setText(mUser.getLastName());
                mAgeEditText.setText(String.valueOf(mUser.getAge()));
            }
        }

        mAddUserButton.setOnClickListener(view -> {
            String firstName = mFirstNameEditText.getText().toString();
            String lastName = mLastNameEditText.getText().toString();
            String ageString = mAgeEditText.getText().toString();
            if (!firstName.isEmpty() && !lastName.isEmpty() && !ageString.isEmpty()) {
                if (operation.equals(ADD)) {
                    User user = new User();
                    user.setFirstName(firstName);
                    user.setLastName(lastName);
                    user.setAge(Integer.valueOf(ageString));
                    mPresenter.addUser(user);
                } else if (operation.equals(UPDATE)) {
                    if (mUser != null) {
                        mUser.setFirstName(firstName);
                        mUser.setLastName(lastName);
                        mUser.setAge(Integer.valueOf(ageString));
                        mPresenter.updateUser(mUser);
                    } else {
                        Log.e(TAG, "mUser is null");
                    }
                } else {
                    Toast.makeText(getActivity(), "wrong operation", Toast.LENGTH_SHORT).show();
                }
            }
        });
    }

    public void setListener(AddUserDialogListener listener) {
        mListener = listener;
    }

    @Override
    public void onAddUserSuccess(User user) {
        Log.i(TAG, "onAddUserSuccess: ");
        if (mListener != null) {
            mListener.onUserAdded(user);
            dismiss();
        }
    }

    @Override
    public void onAddUserError(Throwable throwable) {
        Log.e(TAG, "onAddUserError: " + throwable.getMessage());
        throwable.printStackTrace();
        if (getActivity() != null)
            Toast.makeText(getActivity(), throwable.getMessage(), Toast.LENGTH_LONG).show();
    }

    @Override
    public void onUpdateUserSuccess(User user) {
        Log.i(TAG, "onUpdateUserSuccess: " + user.toString());
        if (mListener != null) {
            mListener.onUserUpdated(user);
            dismiss();
        }
    }

    @Override
    public void onUpdateUserError(Throwable throwable) {
        Log.e(TAG, "onUpdateUserError: " + throwable.getMessage());
        throwable.printStackTrace();
        if (getActivity() != null)
            Toast.makeText(getActivity(), throwable.getMessage(), Toast.LENGTH_SHORT).show();
    }
}
