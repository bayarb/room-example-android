package com.bogdanov.bayar.roomsqlexample.ui.view;

import com.bogdanov.bayar.roomsqlexample.base.BaseView;
import com.bogdanov.bayar.roomsqlexample.model.User;

/**
 * Created by qqq on 02.08.2017.
 */

public interface AddUserDialogView extends BaseView {
    void onAddUserSuccess(User user);
    void onAddUserError(Throwable throwable);

    void onUpdateUserSuccess(User user);
    void onUpdateUserError(Throwable throwable);
}
