package com.bogdanov.bayar.roomsqlexample.di;

import android.arch.persistence.room.Room;
import android.content.Context;

import com.bogdanov.bayar.roomsqlexample.Application;
import com.bogdanov.bayar.roomsqlexample.database.AppDatabase;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

/**
 * Created by qqq on 31.07.2017.
 */

@Module
public class AppModule {

    private Application mApplication;

    public AppModule(Application application) {
        mApplication = application;
    }

    @Singleton
    @Provides
    public Context provideContext() {
        return mApplication.getApplicationContext();
    }

    @Singleton
    @Provides
    public AppDatabase provideAppDatabase() {
        return Room.databaseBuilder(mApplication.getApplicationContext(), AppDatabase.class, "user-database")
                .allowMainThreadQueries()
                .build();
    }
}
