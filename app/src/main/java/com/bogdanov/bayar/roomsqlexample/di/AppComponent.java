package com.bogdanov.bayar.roomsqlexample.di;

import com.bogdanov.bayar.roomsqlexample.ui.activity.MainActivity;
import com.bogdanov.bayar.roomsqlexample.ui.dialog.AddUserDialog;

import javax.inject.Singleton;

import dagger.Component;

/**
 * Created by qqq on 31.07.2017.
 */

@Singleton
@Component(modules = AppModule.class)
public interface AppComponent {
    void inject(MainActivity activity);
    void inject(AddUserDialog dialog);
}
