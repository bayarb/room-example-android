package com.bogdanov.bayar.roomsqlexample.presenter;

import android.util.Log;

import com.bogdanov.bayar.roomsqlexample.base.BasePresenter;
import com.bogdanov.bayar.roomsqlexample.database.AppDatabase;
import com.bogdanov.bayar.roomsqlexample.model.User;
import com.bogdanov.bayar.roomsqlexample.ui.view.AddUserDialogView;

import javax.inject.Inject;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;

/**
 * Created by qqq on 02.08.2017.
 */

public class AddUserPresenter extends BasePresenter<AddUserDialogView> {

    private static final String TAG = AddUserPresenter.class.getName();

    private AppDatabase mAppDatabase;

    @Inject
    public AddUserPresenter(AppDatabase appDatabase) {
        mAppDatabase = appDatabase;
    }

    public void addUser(User user) {
        mAppDatabase.writeUser(user)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(user1 -> {
                    Log.i(TAG, "onNext: " + user1.toString());
                    getView().onAddUserSuccess(user1);
                }, throwable -> {
                    getView().onAddUserError(throwable);
                }, () -> {
                    Log.i(TAG, "onCompleted: ");
                }, disposable -> {
                    Log.i(TAG, "onSubscribe: " + disposable.toString());
                });
    }

    public void updateUser(User user) {
        mAppDatabase.updateUser(user)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(user1 -> {
                    getView().onUpdateUserSuccess(user1);
                }, throwable -> {
                    getView().onUpdateUserError(throwable);
                });
    }
}
