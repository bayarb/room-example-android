package com.bogdanov.bayar.roomsqlexample.presenter;

import android.content.Context;
import android.util.Log;

import com.bogdanov.bayar.roomsqlexample.base.BasePresenter;
import com.bogdanov.bayar.roomsqlexample.database.AppDatabase;
import com.bogdanov.bayar.roomsqlexample.model.User;
import com.bogdanov.bayar.roomsqlexample.ui.view.MainView;

import javax.inject.Inject;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;

/**
 * Created by qqq on 31.07.2017.
 */

public class MainPresenter extends BasePresenter<MainView>{

    private static final String TAG = MainPresenter.class.getName();

    private Context mContext;
    private AppDatabase mAppDatabase;

    @Inject
    public MainPresenter(Context context, AppDatabase appDatabase) {
        mContext = context;
        mAppDatabase = appDatabase;
    }

    public void getAllUsers() {
        mAppDatabase.getAllUsers()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(users -> {
                    Log.i(TAG, "onNext: " + users.size());
                    getView().onGetAllUsersSuccess(users);
                }, throwable -> {
                    Log.e(TAG, "onError: " + throwable.getMessage());
                    throwable.printStackTrace();
                }, () -> {
                    Log.i(TAG, "onComplete: ");
                }, disposable -> {
                    Log.i(TAG, "onSubscribe: " + disposable.toString());
                });
    }

    public void deleteUser(User user) {
        mAppDatabase.deleteUser(user)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(user1 -> {
                    getView().onDeleteUserSuccess(user1);
                }, throwable -> {
                    getView().onDeleteUserError(throwable);
                });
    }
}
